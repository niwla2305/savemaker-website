import { fileURLToPath } from 'url'
import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    rollupOptions: {
      input: {
        index: fileURLToPath(new URL('./index.html', import.meta.url)),
        feedbackThanks: fileURLToPath(new URL('./feedback_thanks.html', import.meta.url)),
      },
    },
  },
})