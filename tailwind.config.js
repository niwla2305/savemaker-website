/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "index.html",
    "feedback_thanks.html"
  ],
  theme: {
    extend: {
      colors: {
        primary: "#00fd00",
        background: "#323232",
        background2: "#414141"
      }
    },
  },
  plugins: [],
}
